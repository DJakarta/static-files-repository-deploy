// eslint-disable-next-line security/detect-child-process
const childProcess = require("child_process");
const del = require("del");
const jsonfile = require("jsonfile");
const isString = require("is-string");

module.exports = {
  /* TODO Add the ability to filter which files get copied from repositories. */
  StaticFilesRepositoryDeployment: class StaticFilesRepositoryDeployment {
    constructor(name, repository, folderPath) {
      this.name = name;
      this.repository = repository;
      this.folderPath = folderPath;
    }
  },
  loadConfigurationFile(filePath) {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    return jsonfile.readFileSync(filePath);
  },
  defaultConfigurationFile: "staticFilesRepositoryDeployments.json",
  updateConfigurationDeployments(configuration) {
    if (!configuration) {
      configuration = this.defaultConfigurationFile;
    }
    if (isString(configuration)) {
      configuration = this.loadConfigurationFile(configuration);
    }
    configuration.forEach((deployment) => {
      this.updateDeployment(deployment.repository, deployment.folderPath);
    });
  },
  updateNamedDeployment(name, configuration) {
    if (!configuration) {
      configuration = this.defaultConfigurationFile;
    }
    if (isString(configuration)) {
      configuration = this.loadConfigurationFile(configuration);
    }
    configuration.filter((deployment) => (deployment.name === name))
      .forEach((deployment) => {
        this.updateDeployment(deployment.repository, deployment.folderPath);
      });
  },
  
  /* TODO Add some mechanism to remember which commit was last used for a deployment to implement
  caching when there is no newer commit available. */
  /* TODO Add the ability to do the work asynchronously and notify when done. */
  /* TODO Add easy to use middleware for Express.js to wait until the deployment is ready before serving request. */
  /* TODO Refactor to accept a deployment object. */
  updateDeployment(repository, folderPath) {
    console.log(`Updating files in ${folderPath} from ${repository}...`);
    del.sync(folderPath);
    childProcess.execFile(
      "git",
      ["clone", repository, folderPath],
      (err, stdout, stderr) => {
        if (err) {
          console.error(err);
        }
        else {
          del.sync(`${folderPath}/.git`);
          console.log(`git stdout:\n ${stdout}`);
          console.log(`git stderr:\n ${stderr}`);
          console.log(`Successfully updated files in ${folderPath} from ${repository}.`);
        }
      },
    );
  },
};