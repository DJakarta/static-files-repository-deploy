# Static files repository deploy

Deploy static files from a git repository with/in [Node.js](https://nodejs.org/) projects.

## Usage
1. Install via `npm install git+https://gitlab.com/DJakarta/static-files-repository-deploy.git`.
2. Require the module with `const staticFilesRepositoryDeploy = require("static-files-repository-deploy");`.
3. [Update](#updating-deployments) a [deployment](#deployments).  
Easiest way to do so is to just `staticFilesRepositoryDeploy.updateConfigurationDeployments()` after having specified the configuration in the `staticFilesRepositoryDeployments.json` file.

### Updating deployments
Deployments can be updated in a few ways:  
1. Using `staticFilesRepositoryDeploy.updateDeployment(repository, folderPath)`.  
It updates the folder files with the specified git URL repository files.  
2. Using `staticFilesRepositoryDeploy.updateConfigurationDeployments(configuration)`.  
It updates the deployments specified in the given configuration.  
If a filename string instead of an usual configuration object is provided, the configuration is loaded from the given file.  
If no configuration parameter is provided, the configuration is loaded from the given file.  
3. Using `staticFilesRepositoryDeploy.updateNamedDeployment(name, configuration)`.  
It is similiar to the `staticFilesRepositoryDeploy.updateConfigurationDeployments(configuration)` option but it only updates the named deployments specified.  

## Concepts
### Deployments
Conceptually, a deployment is a pair of `[repository, folderPath]`.  
It specifies a git repository from which to copy files and a folder path in which to copy the files.  
The deployment object structure is
```js
{
  repository: repository,
  folderPath: folderPath
}
```
There is also an optional `name` string field provided for convenience, to easier refer to given named deployments.  

In order to deploy the same repository to multiple folders, multiple different deployments with the same git repository source must be used.

### Configuration
A configuration is an object that specifies a set of deployments and deployment rules.  
The current structure of a configuration is an array of deployments. This may be changed in the future.
Configurations can be constructed programatically or can be read from given [configuration files](#file-configuration).  

#### File configuration
File configuration is stored is `JSON` files.  
The default configuration filename is `staticFilesRepositoryDeployments.json`, but it can be changed for a given instance of the module by assigning a filename to `staticFilesRepositoryDeploy.defaultConfigurationFile`.  
Configurations may be read from files using `staticFilesRepositoryDeploy.loadConfigurationFile(filename)` manually, but methods usually accept a string defining a configuration filename (or even no parameter at all, using the default configuration filename) instead of a configuration object and automatically load it from the file for convenience.  